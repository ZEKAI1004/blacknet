/*
 * Copyright (c) 2018-2024 Pavel Vasin
 *
 * Licensed under the Jelurida Public License version 1.1
 * for the Blacknet Public Blockchain Platform (the "License");
 * you may not use this file except in compliance with the License.
 * See the LICENSE.txt file at the top-level directory of this distribution.
 */

package ninja.blacknet.network

import io.github.oshai.kotlinlogging.KotlinLogging
import io.ktor.network.sockets.InetSocketAddress as KtorInetSocketAddress
import io.ktor.network.sockets.ServerSocket
import io.ktor.network.sockets.aSocket
import io.ktor.network.sockets.openReadChannel
import io.ktor.network.sockets.openWriteChannel
import java.math.BigInteger
import java.nio.channels.FileChannel
import java.nio.file.NoSuchFileException
import java.nio.file.StandardOpenOption.READ
import java.util.HashSet.newHashSet
import java.util.concurrent.CopyOnWriteArraySet
import kotlin.random.Random
import kotlinx.atomicfu.atomic
import kotlinx.coroutines.Job
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.sync.withLock
import kotlinx.serialization.Serializable
import ninja.blacknet.stateDir
import ninja.blacknet.Config
import ninja.blacknet.Runtime
import ninja.blacknet.ShutdownHooks
import ninja.blacknet.core.*
import ninja.blacknet.crypto.Hash
import ninja.blacknet.crypto.PoS
import ninja.blacknet.db.LedgerDB
import ninja.blacknet.db.PeerDB
import ninja.blacknet.logging.error
import ninja.blacknet.mode
import ninja.blacknet.network.packet.*
import ninja.blacknet.serialization.bbf.binaryFormat
import ninja.blacknet.time.currentTimeMillis
import ninja.blacknet.time.currentTimeSeconds
import ninja.blacknet.util.SynchronizedArrayList
import ninja.blacknet.util.buffered
import ninja.blacknet.util.data
import ninja.blacknet.util.inputStream
import ninja.blacknet.util.replaceFile
import ninja.blacknet.util.rotate

private val logger = KotlinLogging.logger {}

object Node {
    const val NETWORK_TIMEOUT = 90 * 1000L
    const val PROTOCOL_VERSION = 15
    const val MIN_PROTOCOL_VERSION = 12
    private const val DATA_VERSION = 1
    private const val DATA_FILENAME = "node.dat"
    val nonce = Random.nextLong()
    val connections = SynchronizedArrayList<Connection>()
    val listenAddress = CopyOnWriteArraySet<Address>()
    private val nextPeerId = atomic(1L)
    private val queuedPeers = Channel<Address>(Config.instance.outgoingconnections)

    init {
        // All connectors, including listeners and probers
        val connectors = ArrayList<Job>(Config.instance.outgoingconnections + 3 + 1)

        if (Config.instance.listen) {
            try {
                if (!Network.IPv4.isDisabled() || !Network.IPv6.isDisabled()) {
                    connectors.add(
                        listenOnIP()
                    )
                    if (Config.instance.upnp) {
                        Runtime.launch { UPnP.forward() }
                    }
                }
            } catch (e: Throwable) {
            }
        }
        if (Config.instance.tor) {
            if (!Config.instance.listen || Network.IPv4.isDisabled() && Network.IPv6.isDisabled())
                connectors.add(
                    listenOn(Network.LOOPBACK)
                )
            connectors.add(
                Runtime.rotate(Network.Companion::listenOnTor)
            )
        }
        if (Config.instance.i2p) {
            connectors.add(
                Runtime.rotate(Network.Companion::listenOnI2P)
            )
        }
        try {
            val file = stateDir.resolve(DATA_FILENAME)
            FileChannel.open(file, READ).inputStream().buffered().data().use { stream ->
                val version = stream.readInt()
                val bytes = stream.readAllBytes()
                if (version == DATA_VERSION) {
                    val persistent = binaryFormat.decodeFromByteArray(Persistent.serializer(), bytes)
                    persistent.peers.forEach { peer ->
                        queuedPeers.trySend(peer)
                    }
                } else {
                    logger.warn { "Unknown node data version $version" }
                }
            }
        } catch (e: NoSuchFileException) {
            // first run or unlinked file
        } catch (e: Exception) {
            logger.error(e)
        }
        repeat(Config.instance.outgoingconnections) {
            connectors.add(
                Runtime.rotate(::connector)
            )
        }
        connectors.add(
            Runtime.rotate(::prober)
        )
        ShutdownHooks.add {
            runBlocking {
                logger.info { "Unbinding ${connectors.size} connectors" }
                connectors.forEach(Job::cancel)
                val persistent = Persistent(ArrayList(Config.instance.outgoingconnections))
                connections.mutex.withLock {
                    logger.info { "Closing ${connections.list.size} p2p connections" }
                    connections.list.forEach { connection ->
                        // probers ain't interesting
                        if (connection.state == Connection.State.OUTGOING_CONNECTED)
                            persistent.peers.add(connection.remoteAddress)
                        connection.close()
                    }
                }
                logger.info { "Saving node state" }
                replaceFile(stateDir, DATA_FILENAME) {
                    writeInt(DATA_VERSION)
                    write(binaryFormat.encodeToByteArray(Persistent.serializer(), persistent))
                }
            }
        }
    }

    fun newPeerId(): Long {
        return nextPeerId.getAndIncrement()
    }

    private fun nonce(network: Network): Long = when (network) {
        Network.IPv4, Network.IPv6 -> nonce
        else -> Random.nextLong()
    }

    suspend fun outgoing(): Int {
        return connections.count {
            when (it.state) {
                Connection.State.OUTGOING_CONNECTED -> true
                else -> false
            }
        }
    }

    suspend fun incoming(includeWaiting: Boolean = false): Int {
        return if (includeWaiting)
            connections.count {
                when (it.state) {
                    Connection.State.INCOMING_CONNECTED -> true
                    Connection.State.INCOMING_WAITING -> true
                    else -> false
                }
            }
        else
            connections.count {
                when (it.state) {
                    Connection.State.INCOMING_CONNECTED -> true
                    else -> false
                }
            }
    }

    suspend fun connected(): Int {
        return connections.count {
            when (it.state) {
                Connection.State.INCOMING_CONNECTED -> true
                Connection.State.OUTGOING_CONNECTED -> true
                else -> false
            }
        }
    }

    suspend fun isOffline(): Boolean {
        return connections.find { it.state.isConnected() } == null
    }

    fun getMaxPacketSize(): Int {
        return LedgerDB.state().maxBlockSize + PoS.BLOCK_RESERVED_SIZE
    }

    fun getMinPacketSize(): Int {
        return PoS.DEFAULT_MAX_BLOCK_SIZE + PoS.BLOCK_RESERVED_SIZE
    }

    fun isInitialSynchronization(): Boolean {
        return ChainFetcher.isSynchronizing() && PoS.guessInitialSynchronization()
    }

    fun listenOn(address: Address): Job {
        val addr = when (address.network) {
            Network.IPv4, Network.IPv6 -> address.getSocketAddress()
            else -> throw NotImplementedError("Not implemented for " + address.network)
        }
        val server = aSocket(Network.selector).tcp().bind(addr)
        logger.info { "Listening on ${address.debugName()}" }
        return Runtime.launch {
            listenAddress.add(address)
            listener(server)
        }
    }

    private fun listenOnIP(): Job {
        if (Network.IPv4.isDisabled() && Network.IPv6.isDisabled())
            throw IllegalStateException("Both IPv4 and IPv6 are disabled")
        if (Network.IPv4.isDisabled())
            return listenOn(Address.IPv6_ANY(Config.instance.port.toPort()))
        return listenOn(Address.IPv4_ANY(Config.instance.port.toPort()))
    }

    suspend fun connectTo(address: Address, v2: Boolean, prober: Boolean = false): Connection {
        val connection = Network.connect(address, prober)
        connections.mutex.withLock {
            connections.list.add(connection)
            connection.launch()
        }
        if (v2)
            sendHandshake(connection)
        else
            sendVersion(connection, nonce(address.network), prober)
        return connection
    }

    fun sendVersion(connection: Connection, nonce: Long, prober: Boolean) {
        connection.sendPacket(PacketType.Version, if (prober) {
            Version(
                    mode.networkMagic,
                    PROTOCOL_VERSION,
                    currentTimeSeconds(),
                    nonce,
                    UserAgent.prober,
                    Long.MAX_VALUE,
                    ChainAnnounce.GENESIS
            )
        } else {
            val state = LedgerDB.state()
            Version(
                    mode.networkMagic,
                    PROTOCOL_VERSION,
                    currentTimeSeconds(),
                    nonce,
                    UserAgent.string,
                    TxPool.minFeeRate,
                    ChainAnnounce(state.blockHash, state.cumulativeDifficulty)
            )
        })
    }

    fun sendHandshake(connection: Connection) {
        val hello = Hello().apply {
            magic = mode.networkMagic
            version = PROTOCOL_VERSION
            if (connection.state == Connection.State.OUTGOING_WAITING)
                nonce = nonce(connection.remoteAddress.network) //TODO send only when needed
            agent = if (connection.state == Connection.State.PROBER_WAITING)
                UserAgent.prober
            else
                UserAgent.string
            feeFilter = if (connection.state == Connection.State.PROBER_WAITING)
                Long.MAX_VALUE
            else
                TxPool.minFeeRate
        }
        connection.sendPacket(PacketType.Hello, hello)
        if (connection.state != Connection.State.PROBER_WAITING) {
            val state = LedgerDB.state()
            connection.sendPacket(PacketType.ChainAnnounce, ChainAnnounce(state.blockHash, state.cumulativeDifficulty))
        }
    }

    suspend fun announceChain(hash: Hash, cumulativeDifficulty: BigInteger, source: Connection? = null): Int {
        Staker.awaitsNextTimeSlot?.cancel()
        val ann = ChainAnnounce(hash, cumulativeDifficulty)
        return broadcastPacket(PacketType.ChainAnnounce, ann) {
            it != source && it.lastChain.cumulativeDifficulty < cumulativeDifficulty
        }
    }

    suspend fun broadcastBlock(hash: Hash, bytes: ByteArray): Boolean {
        val (status, n) = ChainFetcher.stakedBlock(hash, bytes)
        if (status == Accepted) {
            if (mode.requiresNetwork)
                logger.info { "Announced to $n peers" }
            return true
        } else {
            logger.info { status.toString() }
            return false
        }
    }

    suspend fun broadcastTx(hash: Hash, bytes: ByteArray): Status {
        val currTime = currentTimeSeconds()
        val (status, fee) = TxPool.process(hash, bytes, currTime, false)
        if (status == Accepted) {
            connections.forEach {
                if (it.state.isConnected() && it.checkFeeFilter(bytes.size, fee))
                    it.inventory(hash)
            }
        }
        return status
    }

    suspend fun broadcastInv(unfiltered: UnfilteredInvList, source: Connection? = null): Int {
        var n = 0
        val toSend = ArrayList<Hash>(unfiltered.size)
        connections.forEach {
            if (it != source && it.state.isConnected()) {
                for (i in 0 until unfiltered.size) {
                    val (hash, size, fee) = unfiltered[i]
                    if (it.checkFeeFilter(size, fee))
                        toSend.add(hash)
                }
                if (toSend.size != 0) {
                    it.inventory(toSend)
                    toSend.clear()
                    n += 1
                }
            }
        }
        return n
    }

    private suspend inline fun timeOffset(): Long = connections.mutex.withLock {
        Config.instance.outgoingconnections.let { min ->
            connections.list.fold(
                ArrayList<Long>(min)
            ) { accumulator, element ->
                accumulator.apply {
                    if (element.state == Connection.State.OUTGOING_CONNECTED)
                        add(element.timeOffset)
                }
            }.run {
                if (size >= min) {
                    sort()
                    this[size / 2] // median
                } else {
                    0
                }
            }
        }
    }

    suspend fun warnings(): List<String> {
        val timeOffset = timeOffset()

        return if (timeOffset >- PoS.TIME_SLOT && timeOffset <+ PoS.TIME_SLOT)
            emptyList()
        else
            listOf("Please check your system clock. Many peers report different time.")
    }

    private suspend fun broadcastPacket(type: PacketType, packet: Packet, filter: (Connection) -> Boolean = { true }): Int {
        logger.debug { "Broadcasting $type" }
        var n = 0
        val bytes = buildPacket(type, packet)
        connections.forEach {
            if (it.state.isConnected() && filter(it)) {
                it.sendPacket(bytes.copy())
                n += 1
            }
        }
        bytes.release()
        return n
    }

    private suspend fun listener(server: ServerSocket) {
        while (true) {
            val socket = server.accept()
            val remoteAddress = Network.address(socket.remoteAddress as KtorInetSocketAddress)
            val localAddress = Network.address(socket.localAddress as KtorInetSocketAddress)
            if (!localAddress.isLocal())
                listenAddress.add(localAddress)
            val connection = Connection(socket, socket.openReadChannel(), socket.openWriteChannel(true), remoteAddress, localAddress, Connection.State.INCOMING_WAITING)
            addConnection(connection)
        }
    }

    suspend fun addConnection(connection: Connection) {
        if (!haveSlot()) {
            logger.info { "Too many connections, dropping ${connection.debugName()}" }
            connection.close()
            return
        }
        connections.mutex.withLock {
            connections.list.add(connection)
            connection.launch()
        }
    }

    private suspend fun haveSlot(): Boolean {
        return if (incoming(true) < Config.instance.incomingconnections)
            true
        else
            evictConnection()
    }

    private suspend fun evictConnection(): Boolean {
        val candidates = connections.filter { it.state.isIncoming() }.asSequence()
                .sortedBy { if (it.ping != 0L) it.ping else Long.MAX_VALUE }.drop(4)
                .sortedByDescending { it.lastTxTime }.drop(4)
                .sortedByDescending { it.lastBlockTime }.drop(4)
                .sortedBy { it.connectedAt }.drop(4)
                .toMutableList()

        //TODO network groups

        if (candidates.isEmpty())
            return false

        val connection = candidates.random()
        logger.info { "Evicting ${connection.debugName()}" }
        connection.close()
        return true
    }

    private suspend fun getFilter(): HashSet<Address> {
        val filter = newHashSet<Address>(connections.size() + listenAddress.size)
        connections.forEach { filter.add(it.remoteAddress) }
        listenAddress.forEach { filter.add(it) }
        return filter
    }

    private suspend fun connector() {
        val filter = getFilter()
        val address = queuedPeers.tryReceive().getOrNull() ?: PeerDB.getCandidate { address, _ -> !filter.contains(address) }
        if (address == null) {
            val outgoing = outgoing()
            logger.info { "Don't have candidates in PeerDB. $outgoing connections, max ${Config.instance.outgoingconnections}" }
            delay(15 * 60 * 1000L)
            return
        }

        val time = currentTimeMillis()

        try {
            val connection = connectTo(address, v2 = true)
            connection.job.join()
            // try v1 if accepted without reply
            if (connection.totalBytesRead == 0L)
                connectTo(address, v2 = false).job.join()
        } catch (e: Throwable) {
            PeerDB.failed(address, time / 1000L)
        }

        val x = 4 * 1000L - (currentTimeMillis() - time)
        if (x > 0L)
            delay(x) // 請在繼續之前等待或延遲
    }

    /**
     * Peer network prober is a tool that periodically tries to establish an
     * outgoing connection in order to provide statistic for the peer database
     * prober.
     */
    private suspend fun prober() {
        delay(4 * 60 * 1000L)

        // Await peer network address announce
        if (PeerDB.size() < PeerDB.MAX_SIZE / 2)
            return

        // Await while connectors are working
        if (outgoing() < Config.instance.outgoingconnections)
            return

        val filter = getFilter()
        val time = currentTimeMillis()
        val address = PeerDB.getCandidate { address, entry ->
            (time / 1000L > entry.lastTry + 4 * 60 * 60) && !filter.contains(address)
        } ?: return

        try {
            val connection = connectTo(address, v2 = true, prober = true)
            connection.job.join()
            // try v1 if accepted without reply
            if (connection.totalBytesRead == 0L)
                connectTo(address, v2 = false, prober = true)
        } catch (e: Throwable) {
            PeerDB.failed(address, time / 1000L)
        }
    }

    @Serializable
    private class Persistent(
        val peers: ArrayList<Address>
    )
}
